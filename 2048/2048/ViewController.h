//
//  ViewController.h
//  2048
//
//  Created by Amanda Kobiolka on 2/7/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * arrayOfLabels;

@property (strong, nonatomic) IBOutlet UIButton *upButton;

@property (strong, nonatomic) IBOutlet UIButton *leftButton;
@property (strong, nonatomic) IBOutlet UIButton *downButton;
@property (strong, nonatomic) IBOutlet UIButton *rightButton;
@property (strong, nonatomic) IBOutlet UILabel *TitleLabel;

@property (strong, nonatomic) IBOutlet UIButton *resetGame;



@end

