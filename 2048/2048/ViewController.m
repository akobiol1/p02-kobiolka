//
//  ViewController.m
//  2048
//
//  Created by Amanda Kobiolka on 2/7/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
NSInteger matrix[16];

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)resetValues:(id)sender {
    for(int i = 0; i < 16; i++) {
        matrix[i] = 0;
        UILabel *label =_arrayOfLabels[i];
        label.text = @"0";
    }
    matrix[4] = 2;
    UILabel *label =_arrayOfLabels[4];
    label.text = @"2";
    
}

/*--------------------------------Down----------------------------*/

- (IBAction)moveDown:(id)sender {

    
    //Shift everything down
    for(int i = 12; i < 16; i++) {
        for(int j = 0; j < 4; j++) { //keep shifting
            int index = i;
            NSLog(@"Index is set to %i)", index);
            while ((index-4) >= 0) {
                if (matrix[index] == 0) {
                    NSLog(@"if (matrix[%i] == 0)", index);
                    matrix[index] = matrix[index-4];
                    NSLog(@"matrix[%i] = %i", index, matrix[index-4]);
                    matrix[index-4] = 0;
                    NSLog(@"matrix[%i] = 0;", index-4);
                    index -= 4;
                } else {
                    index -= 4;
                }
                NSLog(@"End of while, index is now: %i", index);
            }
        }
    }

    
    //Combine values if you can, again

    for(int i = 12; i < 16; i++) {
        if(matrix[i] == matrix[i-4]) {
            matrix[i] += matrix[i-4]; //double the value
            matrix[i-4] = 0; //zero out old value
        } else if (matrix[i-4] == matrix[i-8]) {
            matrix[i-4] += matrix[i-8];
            matrix[i-8] = 0;
            
        } else if (matrix[i-8] == matrix[i-12]) {
            matrix[i-8] += matrix[i-12];
            matrix[i-12] = 0;
            
        }
    }
    //Shift everything down
    for(int i = 12; i < 16; i++) {
        for(int j = 0; j < 4; j++) { //keep shifting
            int index = i;
            NSLog(@"Index is set to %i)", index);
            while ((index-4) >= 0) {
                if (matrix[index] == 0) {
                    NSLog(@"if (matrix[%i] == 0)", index);
                    matrix[index] = matrix[index-4];
                    NSLog(@"matrix[%i] = %i", index, matrix[index-4]);
                    matrix[index-4] = 0;
                    NSLog(@"matrix[%i] = 0;", index-4);
                    index -= 4;
                } else {
                    index -= 4;
                }
                NSLog(@"End of while, index is now: %i", index);
            }
        }
    }

    
    //Adding a new tile
    NSMutableArray *zeroValues = [NSMutableArray array];
    
    for(int i = 0; i < 16; i++) {
        if (matrix[i] == 0){
            [zeroValues addObject:[NSNumber numberWithInt:i]];
        }
    }
    
    int size = [zeroValues count];
    int r = arc4random() % size;
    
    int theNumber = [[zeroValues objectAtIndex:r] integerValue];
    matrix[theNumber] = 2;
    
    //Plug the new values back into the matrix
    for(int i = 0; i < 16; i++) {
        UILabel *label =_arrayOfLabels[i];
        label.text = [NSString stringWithFormat:@"%i", matrix[i]];
        
    }
}
/*--------------------------------LEFT----------------------------*/
- (IBAction)moveLeft:(id)sender {
    
    //Shift everything left
    for(int i = 0; i < 16; i+=4) {
        for(int j = 0; j < 4; j++) {
            int index = i;
            while (index < (i+3)) {
                if (matrix[index] == 0) {
                    matrix[index] = matrix[index+1];
                    matrix[index+1] = 0;
                    index += 1;
                } else {
                    index += 1;
                }
            }
        }
    }
    
    //Combine values if you can
    for(int i = 0; i < 16; i+=4) {
        if(matrix[i] == matrix[i+1]) {
            matrix[i] += matrix[i+1]; //double the value
            matrix[i+1] = 0; //zero out old value
        } else if (matrix[i+1] == matrix[i+2]) {
            matrix[i+1] += matrix[i+2];
            matrix[i+2] = 0;
            
        } else if (matrix[i+2] == matrix[i+3]) {
            matrix[i+2] += matrix[i+3];
            matrix[i+3] = 0;
        }
    }
    
    //Shift everything left again
    for(int i = 0; i < 16; i+=4) {
        for(int j = 0; j < 4; j++) {
            int index = i;
            while (index < (i+3)) {
                if (matrix[index] == 0) {
                    matrix[index] = matrix[index+1];
                    matrix[index+1] = 0;
                    index += 1;
                } else {
                    index += 1;
                }
            }
        }
    }
    
    //Adding a new tile
    NSMutableArray *zeroValues = [NSMutableArray array];
    
    for(int i = 0; i < 16; i++) {
        if (matrix[i] == 0){
            [zeroValues addObject:[NSNumber numberWithInt:i]];
        }
    }
    
    int size = [zeroValues count];
    if (size == 0) {
        
        
    }
    
    int r = arc4random() % size;
    
    int theNumber = [[zeroValues objectAtIndex:r] integerValue];
    matrix[theNumber] = 2;
    
    //Plug the new values back into the matrix
    for(int i = 0; i < 16; i++) {
        UILabel *label =_arrayOfLabels[i];
        label.text = [NSString stringWithFormat:@"%i", matrix[i]];
    }

}

/*--------------------------------RIGHT----------------------------*/
- (IBAction)moveRight:(id)sender {
    
    //Shift everything right
    for(int i = 3; i < 16; i+=4) {
        NSLog(@"Index: %i", i);
        for(int j = 0; j < 4; j++) {
            int index = i;
            while (index > (i-3)) {
                if (matrix[index] == 0) {
                    NSLog(@"if (matrix[%i] == 0)", index);
                    matrix[index] = matrix[index-1];
                    NSLog(@"matrix[%i] = %i", index, matrix[index-1]);
                    matrix[index-1] = 0;
                    NSLog(@"matrix[%i] = 0;", index-1);
                    index -= 1;
                    NSLog(@"End of while, index is now: %i", index);
                } else {
                    index -= 1;
                }
            }
        }
    }
    
    
    //Combine values if you can
    for(int i = 3; i < 16; i+=4) {
        if(matrix[i] == matrix[i-1]) {
            matrix[i] += matrix[i-1]; //double the value
            matrix[i-1] = 0; //zero out old value
        } else if (matrix[i-1] == matrix[i-2]) {
            matrix[i-1] += matrix[i-2];
            matrix[i-2] = 0;
            
        } else if (matrix[i-2] == matrix[i-3]) {
            matrix[i-2] += matrix[i-3];
            matrix[i-3] = 0;
        }
    }

    //Shift everything right
    for(int i = 3; i < 16; i+=4) {
        NSLog(@"Index: %i", i);
        for(int j = 0; j < 4; j++) {
            int index = i;
            while (index > (i-3)) {
                if (matrix[index] == 0) {
                    NSLog(@"if (matrix[%i] == 0)", index);
                    matrix[index] = matrix[index-1];
                    NSLog(@"matrix[%i] = %i", index, matrix[index-1]);
                    matrix[index-1] = 0;
                    NSLog(@"matrix[%i] = 0;", index-1);
                    index -= 1;
                    NSLog(@"End of while, index is now: %i", index);
                } else {
                    index -= 1;
                }
            }
        }
    }
    
    //Adding a new tile
    NSMutableArray *zeroValues = [NSMutableArray array];
    
    for(int i = 0; i < 16; i++) {
        if (matrix[i] == 0){
            [zeroValues addObject:[NSNumber numberWithInt:i]];
        }
    }
    
    int size = [zeroValues count];
    int r = arc4random() % size;
    
    int theNumber = [[zeroValues objectAtIndex:r] integerValue];
    matrix[theNumber] = 2;
    
    //Plug the new values back into the matrix
    for(int i = 0; i < 16; i++) {
        UILabel *label =_arrayOfLabels[i];
        label.text = [NSString stringWithFormat:@"%i", matrix[i]];
    }

}

/*--------------------------------UP----------------------------*/
- (IBAction)moveUp:(id)sender {
 
 /*   NSLog(@"Starting matrix\n");
    NSLog(@"%i %i %i %i", matrix[0], matrix[1], matrix[2], matrix[3]);
    NSLog(@"%i %i %i %i", matrix[4], matrix[5], matrix[6], matrix[7]);
    NSLog(@"%i %i %i %i", matrix[8], matrix[9], matrix[10], matrix[11]);
    NSLog(@"%i %i %i %i", matrix[12], matrix[13], matrix[14], matrix[15]);*/
    
    //Shift everything up
    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 4; j++) {
            int index = i;
            while (index < 16) {
                if (matrix[index] == 0) {
                    matrix[index] = matrix[index+4];
                    matrix[index+4] = 0;
                    index += 4;
                } else {
                    index += 4;
                }
            }
        }
    }

    
    //Combine values if you can
    for(int i = 0; i < 4; i++) {
        if(matrix[i] == matrix[i+4]) {
            matrix[i] += matrix[i+4]; //double the value
            matrix[i+4] = 0; //zero out old value
        } else if (matrix[i+4] == matrix[i+8]) {
            matrix[i+4] += matrix[i+8];
            matrix[i+8] = 0;
            
        } else if (matrix[i+8] == matrix[i+12]) {
            matrix[i+8] += matrix[i+12];
            matrix[i+12] = 0;
        }
    }
    
    
    //Adding a new tile
    NSMutableArray *zeroValues = [NSMutableArray array];
    
    for(int i = 0; i < 16; i++) {
        if (matrix[i] == 0){
            [zeroValues addObject:[NSNumber numberWithInt:i]];
        }
    }
    
    int size = [zeroValues count];
    int r = arc4random() % size;

    int theNumber = [[zeroValues objectAtIndex:r] integerValue];
    matrix[theNumber] = 2;
    
    //Plug the new values back into the matrix
    for(int i = 0; i < 16; i++) {
        UILabel *label =_arrayOfLabels[i];
        label.text = [NSString stringWithFormat:@"%i", matrix[i]];
    }
    
}










@end
